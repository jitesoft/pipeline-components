spec:
  inputs:
    stage:
      type: string
      description: "Stage to run job in, defaults to 'test'."
      default: "test"
    phpunit_xml:
      type: string
      description: "Path (or name of) phpunit xml file, defaults to 'phpunit.xml'."
      default: "phpunit.xml"
    skip_versions:
      type: string
      default: "7.4"
      description: "Comma separated list of php versions to skip, defaults to 7.4. Available versions currently: 8.2, 8.1, 8.0, 7.4"
    run_on_merge_request:
      type: boolean
      default: true

---

php.test.phpunit:
  parallel:
    matrix:
      - { PHP_VERSION: "7.4", SKIP_PATTERN: '/(,|^)7{1}[.]4{1}(,|\z)/' }
      - { PHP_VERSION: "8.0", SKIP_PATTERN: '/(,|^)8{1}[.]0{1}(,|\z)/' }
      - { PHP_VERSION: "8.1", SKIP_PATTERN: '/(,|^)8{1}[.]1{1}(,|\z)/' }
      - { PHP_VERSION: "8.2", SKIP_PATTERN: '/(,|^)8{1}[.]2{1}(,|\z)/' }
  stage: $[[ inputs.stage ]]
  image: registry.gitlab.com/jitesoft/dockerfiles/phpunit:${PHP_VERSION}
  variables:
    XDEBUG_MODE: coverage
  artifacts:
    reports:
      junit: report.xml
      coverage_report:
        coverage_format: cobertura
        path: coverage.cobertura.xml
  coverage: /^\s*Lines:\s*\d+.\d+\%/
  script:
    - composer install --prefer-dist --no-progress
    - if [ -f ./vendor/bin/phpunit ]; then ./vendor/bin/phpunit --log-junit report.xml --configuration $[[ inputs.phpunit_xml ]] --colors=never  --coverage-text --coverage-cobertura=coverage.cobertura.xml; fi
    - if [ ! -f ./vendor/bin/phpunit ]; then phpunit --log-junit report.xml --configuration $[[ inputs.phpunit_xml ]] --colors=never  --coverage-text --coverage-cobertura=coverage.cobertura.xml; fi
  rules:
    # Allow disabling all with 'DISABLE_TEST' variable.
    - if: $DISABLE_TEST
      when: never
    # Make use of the 'SKIP_INDICATOR' variable to decide if the matrix job should run.
    - if: '"$[[ inputs.skip_versions ]]" =~ $SKIP_PATTERN'
      when: never
    # Run on merge requests (and external) if run_on_merge_request is true.
    - if: '"$[[ inputs.run_on_merge_request ]]" == "true" && $CI_PIPELINE_SOURCE == "external_pull_request_event"'
      when: always
    - if: '"$[[ inputs.run_on_merge_request ]]" == "true" && $CI_PIPELINE_SOURCE == "merge_request_event"'
      when: always
    # Run on branches.
    - if: '$CI_COMMIT_BRANCH'
      when: always
    - when: never
